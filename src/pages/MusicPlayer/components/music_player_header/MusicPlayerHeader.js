import "./style.css";
import AddAllButton from "../../../../components/AddAllButton/AddAllButton";
import PlayAllButton from "../../../../components/PlatAllButton/PlayAllButton";
import TrackNumber from "../../../../components/TrackNumber/TrackNumber";
import Filter from "../../../../components/Filter/Filter";
import {useState} from "react";


export default function MusicPlayerHeader({song}) {
    let [playAllList, setPlayAllList] = useState([
            {
                id: 1,
                name: "Play All",
            },
            {
                id: 2,
                name: "Play 1",
            },
            {
                id: 3,
                name: "Play 2",
            },
        ]
    )
    let [addAllList, setAddAllList] = useState([
            {
                id: 1,
                name: "Add All",
            },
            {
                id: 2,
                name: "Add 1",
            },
            {
                id: 3,
                name: "Add 2",
            },
        ]
    )
    let [trackNumberList, setTrackNumberList] = useState([
        {
            id: 1,
            name: "Track Number",
        },
        {
            id: 2,
            name: "Track Number 1",
        },
        {
            id: 3,
            name: "Track Number 2",
        }
    ])

    const [selectedPlayOption, setSelectedPlayOption] = useState("Play all");
    const [selectedAddOption, setSelectedAddOption] = useState("Add all");
    const [selectedTrackNumber, setSelectedTrackNumber] = useState("Track Number");

    //when select dropdown item, show this in dropdown and show Timeout result in console
    const handleSelectPlay = (option) => {
        setSelectedPlayOption(option);
        if(selectedPlayOption === "Play all"){
            song.forEach((song, index) => {
                setTimeout(() => {
                    console.log(`Playing: ${song.songName} - ${song.artistName}`);
                }, 3000);
            });
        }
        console.log(`${option} clicked`)
    };

    const handleSelectAdd = (option) => {
        setSelectedAddOption(option)
        //if select add all this all songs are added in new array
            if(selectedAddOption === "Add all"){
                let addArray=[...song]
                console.log(addArray,"Add all in new array")
        }
        console.log(`${option} clicked`)
    };

    const handleSelectTrackNumber = (option) => {
        setSelectedTrackNumber(option)
        console.log(`${option} clicked`)
    };


    return (
        <div className="header">
            <div className="header_b1">
                <PlayAllButton options={playAllList}
                               onSelect={handleSelectPlay} selectedOption={selectedPlayOption} song={song}/>
                <AddAllButton options={addAllList}
                              onSelect={handleSelectAdd} selectedOption={selectedAddOption}/>
            </div>
            <div className="header_b2">
                <TrackNumber options={trackNumberList}
                             onSelect={handleSelectTrackNumber} selectedOption={selectedTrackNumber}/>
                <Filter/>
            </div>
        </div>
    )
}
