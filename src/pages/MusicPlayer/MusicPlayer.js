import "./style.css";
import {useState} from "react";
import MusicPlayerHeader from "./components/music_player_header/MusicPlayerHeader";
import SongList from "../../components/SongList/SongList";
import MusicUploadForm from "../../components/MusicUploadForm/MusicUploadForm";
import song1 from "../../assets/audio/bad-bitch-rap-remix-132117.mp3";
import song2 from "../../assets/audio/let-it-go-12279.mp3";
import song3 from "../../assets/audio/quotchillquot-153588.mp3";
import song4 from "../../assets/audio/this-future-bass-172043.mp3";
import song5 from "../../assets/audio/xan-gotti-trap-shit-freestyle-121775.mp3";

export default function MusicPlayer() {
    const [songs, setSongs] = useState([
        {
            id: 1,
            song:song1,
            songName: 'Song 1',
            artistName: 'Artist 1',
            trackNumber: 1,
        },
        {
            id: 2,
            song:song2,
            songName: 'Song 2',
            artistName: 'Artist 2',
            trackNumber: 2,
        },
        {
            id: 3,
            song:song3,
            songName: 'Song 3',
            artistName: 'Artist 3',
            trackNumber: 3,
        },
        {
            id: 4,
            song:song4,
            songName: 'Song 4',
            artistName: 'Artist 4',
            trackNumber: 4,
        },
        {
            id: 5,
            song:song5,
            songName: 'Song 5',
            artistName: 'Artist 5',
            trackNumber: 5,
        },
        {
            id: 6,
            song:song1,
            songName: 'Song 6',
            artistName: 'Artist 6',
            trackNumber: 6,
        },
    ]);


    return (
        <div className="container">
            <div className="music_player_container">
                <MusicPlayerHeader song={songs}/>
                <div>
                    <SongList songs={songs} />
                    <MusicUploadForm songs={songs}  setSongs={setSongs}/>
                </div>
            </div>
        </div>
    );


}
