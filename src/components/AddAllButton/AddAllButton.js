import React from 'react';
import Dropdown from "../../cors/dropdown/dropdown";

export default function AddAllButton({options, onSelect, selectedOption}) {

    return (
        <Dropdown options={options} onSelect={onSelect} selectedOption={selectedOption}/>
    )

}
