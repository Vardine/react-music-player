import React, {useRef, useState} from 'react';
import "../SongList/style.css";
import list_icon from "../../assets/images/list-icon.png";
import play_icon from "../../assets/images/play-icon.png";
import pause_icon from "../../assets/images/pause-icon.png";
import heart_icon from "../../assets/images/heart-icon.png";
import heart_icon_not_full from "../../assets/images/heart-icon-not-full.png";
import check_icon from "../../assets/images/check-icon.png";
import share_icon from "../../assets/images/share-icon.png";
import arrow_down_icon from "../../assets/images/down-arrow-icon.png";


export default function SongRow({song, index}) {
    const [isPlaying, setIsPlaying] = useState(false);
    const audioRef = useRef(null);
    const [addFavorite, setAddFavorite] = useState(false)


    //Play and pause audio
    const togglePlayPause = (e) => {
        if (isPlaying) {
            audioRef.current.pause();
            setIsPlaying(false);
        } else {
            audioRef.current.play();
        }
        setIsPlaying(!isPlaying);
    };


    //toggle favorite icon
    const toggleAddFavorite = () => {
        setAddFavorite(!addFavorite);
    };

    return (
        <tr>
            <td className="first">
                <img src={list_icon} alt="list_icon"/>
                {!isPlaying ?
                    <img src={play_icon} alt="play_icon"
                         onClick={togglePlayPause}
                    />
                    :
                    <img src={pause_icon} alt="play_icon"
                         onClick={togglePlayPause}
                    />
                }
                <audio ref={audioRef} src={song.song}/>
            </td>
            <td>{song.songName}</td>
            <td>{song.artistName}</td>
            <td>{song.trackNumber}</td>
            <td className="last">
                {
                    !addFavorite ?
                        <img src={heart_icon} alt="heart_icon" onClick={toggleAddFavorite}/>
                        :
                        <img src={heart_icon_not_full} alt="heart_icon" onClick={toggleAddFavorite}/>
                }
                <img src={check_icon} alt="check_icon"/>
                <img src={share_icon} alt="share_icon"/>
                <img src={arrow_down_icon} alt="arrow_icon"/>
            </td>
        </tr>
    )
}
