import "./style.css";
import search_icon from "../../assets/images/search-icon.png";


export default function Filter(){
    return (
        <div className="filter_b">
            <img src={search_icon} alt="search_icon"/>
            <input type="text" placeholder="Filter"/>
        </div>
    )
}