import React, {useState} from 'react';
import "./style.css";
import axios from "axios";


export default function MusicUploadForm({songs, setSongs}) {
    const [selectedFile, setSelectedFile] = useState(null);
    const [uploading, setUploading] = useState(false);

    //select file from computer
    const handleFileChange = (e) => {
        const file = e.target.files[0];
            setSelectedFile(file);
    };

    //when file upload show in console message
    const handleFileUpload = (file) => {
        axios.post('/api/upload', file)
            .then(response => {
                console.log('File uploaded successfully', response.data);
                setUploading(false);
            })
            .catch(error => {
                console.error('Error uploading file', error);
                setUploading(false);
            });
        console.log(`Uploading file: ${file.name}`);
    };


    //Upload file and add file to songList
    const handleUpload = () => {
        if (selectedFile) {
            setUploading(true);
            setTimeout(() => {
                setUploading(false);
                handleFileUpload(selectedFile);
                setSelectedFile(null);
                songs.push(
                    {
                        song: URL.createObjectURL(selectedFile),
                        songName: 'Uploaded Song',
                        artistName: 'Artist',
                        trackNumber: `${songs.length + 1}`
                    }
                )
                setSongs([...songs])
            }, 3000);
        }
        console.log("a file is in the process of being uploaded")
    };


    return (
        <div className="music_upload_container">
            <input type="file" accept=".mp3, .wav" onChange={handleFileChange}/>
            <button onClick={handleUpload} disabled={!selectedFile || uploading} className="upload_btn">
                {uploading ? 'Uploading...' : 'Upload'}
            </button>
        </div>
    );
};
