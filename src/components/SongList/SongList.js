import React from 'react';
import SongRow from '../SongRow/SongRow';
import "./style.css";

export default function SongList ({ songs }) {

    return(
        <div className="songList_container">
            <div className="songList_container_content">
                <table id="songs">
                    <tr>
                        <th> </th>
                        <th>Song Name</th>
                        <th>Artist Name</th>
                        <th>Track</th>
                        <th> </th>
                    </tr>
                    {
                        songs.map((item,index)=>{
                            return(
                                <SongRow  song={item} key={index} index={index}/>
                            )
                        })
                    }
                </table>
            </div>
        </div>
    )
}
