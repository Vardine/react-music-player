import React, {useEffect, useRef, useState} from 'react';
import "./style.css";
import arrow_down from "../../assets/images/down-arrow-icon.png";

export default function Dropdown({options, onSelect, selectedOption}) {
    const [isOpen, setIsOpen] = useState(false);
    const dropdownRef = useRef(null);

    //open close dropdown
    const handleToggleDropdown = () => {
        setIsOpen(!isOpen)
    };

    //close dropdown when click outside
    const handleClickOutside = (event) => {
        if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
            setIsOpen(false);
        }
    };

    useEffect(() => {
        document.addEventListener('mousedown', handleClickOutside);

        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, []);


    //select dropdown item and after select close dropdown
    const handleOptionClick = (option, index) => {
        onSelect(option);
        setIsOpen(false);
    };

    return (
        <div className="play_all_btn" ref={dropdownRef}>
            <button className="dropdown-toggle" onClick={handleToggleDropdown}>
                {selectedOption ? selectedOption : "Select"}
            </button>
            <div className="arrow_b" onClick={handleToggleDropdown}>
                {
                    !isOpen ?
                        <img src={arrow_down} alt="arrow_down"/>
                        :
                        <img src={arrow_down} alt="arrow_down" style={{transform: "Rotate(180deg)"}}/>
                }
            </div>
            {isOpen && (
                <div className="dropdown_content">
                    {options.map((option, index) => (
                        <div
                            key={option.id}
                            className="dropdown_content_item"
                            onClick={() => handleOptionClick(option.name)}
                        >
                            {option.name}
                        </div>
                    ))}
                </div>
            )}
        </div>
    );
};
