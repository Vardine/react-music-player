import './App.css';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import MusicPlayer from "./pages/MusicPlayer/MusicPlayer";


function App() {
  return (
      <div className="App">
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<MusicPlayer/>}/>
          </Routes>
        </BrowserRouter>
      </div>
  );
}

export default App;
